const Browser = require('zombie');
const {url, data} = require('./kramer_tests_set_client.json');

data.forEach((piece, i) => {
  describe(`Visits page and clicks the button, entry #${i + 1}`, function() {
    this.timeout(30000);
    const browser = new Browser();
    before(() => browser.visit(url + piece.input));

    it('should check the proper title on visit', () => {
      browser.assert.text('title', JSON.stringify(piece.output.result));
    });

    it('should check the proper title on button click', async () => {
      await browser.pressButton('Вычислить!');
      browser.assert.text('title', JSON.stringify(piece.output).replace('d",', 'd", '));
    });//needs that replace part if we don't want to redesign the json, the page title has a comma after "resulted" unlike the result of JSON.stringify 
  });
});
